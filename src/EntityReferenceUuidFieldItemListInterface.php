<?php

namespace Drupal\entity_reference_uuid;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;

/**
 * Interface for entity reference lists of field items.
 */
interface EntityReferenceUuidFieldItemListInterface extends EntityReferenceFieldItemListInterface {

}
