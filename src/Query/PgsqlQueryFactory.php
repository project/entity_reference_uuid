<?php

namespace Drupal\entity_reference_uuid\Query;

use Drupal\Core\Entity\Query\Sql\pgsql\QueryFactory as BaseQueryFactory;

/**
 * PostgreSQL specific entity query implementation.
 */
class PgsqlQueryFactory extends BaseQueryFactory {

}
